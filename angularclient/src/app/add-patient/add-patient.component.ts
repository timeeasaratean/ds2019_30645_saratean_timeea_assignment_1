import { Component, OnInit } from '@angular/core';
import {Medication} from "../models/medication";
import {DoctoService} from "../services/docto.service";
import {Router} from "@angular/router";
import {Patient} from "../models/patient";
import {Caregiver} from "../models/caregiver";
import {Observable} from "rxjs";
import {User} from "../models/user";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.css']
})
export class AddPatientComponent implements OnInit {

  patient: Patient = new Patient();
  submitted: boolean;
  caregivers: Observable<Caregiver[]>;
  user: User =new User();
  idCaregiver: number;
  caregiver= new Caregiver();

  constructor(private doctoService: DoctoService,
              private router: Router) {
  }

  ngOnInit() {
    this.caregivers=this.doctoService.getCaregivers();
    this.idCaregiver = 1;

  }

  save() {
    let user = this.user;
    let patient= this.patient;
    this.doctoService.addPatient(patient, user, this.idCaregiver)
      .subscribe(data => console.log(data), error => console.log(error));
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/doctor/patients']);
  }

}
