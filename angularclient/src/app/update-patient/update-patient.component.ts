import { Component, OnInit } from '@angular/core';
import {Medication} from "../models/medication";
import {ActivatedRoute, Router} from "@angular/router";
import {DoctoService} from "../services/docto.service";
import {Patient} from "../models/patient";

@Component({
  selector: 'app-update-patient',
  templateUrl: './update-patient.component.html',
  styleUrls: ['./update-patient.component.css']
})
export class UpdatePatientComponent implements OnInit {

  id: number;
  patient: Patient;
  submitted= false;

  constructor(private route: ActivatedRoute,private router: Router,
              private doctoService: DoctoService) { }

  ngOnInit() {
    this.patient = new Patient();

    this.id = this.route.snapshot.params['id'];

    this.doctoService.getPatient(this.id)
      .subscribe(data => {
        console.log(data)
        this.patient = data;
      }, error => console.log(error));
  }

  updatePatient() {
    this.doctoService.updatePatient(this.id, this.patient)
      .subscribe(data => console.log(data), error => console.log(error));
    this.patient = new Patient();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.updatePatient();
  }

  gotoList() {
    this.router.navigate(['/doctor/patients']);
  }

}
