import { Component, OnInit } from '@angular/core';
import {Medication} from "../models/medication";
import {ActivatedRoute, Router} from "@angular/router";
import {DoctoService} from "../services/docto.service";

@Component({
  selector: 'app-update-medication',
  templateUrl: './update-medication.component.html',
  styleUrls: ['./update-medication.component.css']
})
export class UpdateMedicationComponent implements OnInit {

  id: number;
  medication: Medication;
  submitted= false;

  constructor(private route: ActivatedRoute,private router: Router,
              private doctoService: DoctoService) { }

  ngOnInit() {
    this.medication = new Medication();

    this.id = this.route.snapshot.params['id'];

    this.doctoService.getMedication(this.id)
      .subscribe(data => {
        console.log(data)
        this.medication = data;
      }, error => console.log(error));
  }

  updateMedication() {
    this.doctoService.updateMedication(this.id, this.medication)
      .subscribe(data => console.log(data), error => console.log(error));
    this.medication = new Medication();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.updateMedication();
  }

  gotoList() {
    this.router.navigate(['/doctor/medicamentationList']);
  }

}
