import {Component, OnInit} from '@angular/core';
import {Medication} from "../models/medication";
import {DoctoService} from "../services/docto.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-medication',
  templateUrl: './add-medication.component.html',
  styleUrls: ['./add-medication.component.css']
})
export class AddMedicationComponent implements OnInit {

  medication: Medication = new Medication();
  submitted: boolean;

  constructor(private doctoService: DoctoService,
              private router: Router) {
  }

  ngOnInit() {
  }

  save() {
    this.doctoService.createMedication(this.medication)
      .subscribe(data => console.log(data), error => console.log(error));
    this.medication = new Medication();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/doctor/medicamentationList']);
  }

}
