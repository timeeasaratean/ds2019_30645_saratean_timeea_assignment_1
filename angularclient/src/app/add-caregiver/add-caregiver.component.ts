import {Component, OnInit} from '@angular/core';
import {DoctoService} from "../services/docto.service";
import {Router} from "@angular/router";
import {Caregiver} from "../models/caregiver";
import {User} from "../models/user";
import {Observable} from "rxjs";
import {Doctor} from "../models/doctor";

@Component({
  selector: 'app-add-caregiver',
  templateUrl: './add-caregiver.component.html',
  styleUrls: ['./add-caregiver.component.css']
})
export class AddCaregiverComponent implements OnInit {

  caregiver: Caregiver = new Caregiver();
  submitted: boolean;
  user: User =new User();
  idDoctor: number;
  doctors: Observable<Doctor[]>;

  constructor(private doctoService: DoctoService,
              private router: Router) {
  }

  ngOnInit() {
    this.idDoctor = 1;
    this.doctors = this.doctoService.getDoctors();
  }

  save() {
    this.doctoService.createCaregiver(this.caregiver, this.user, this.idDoctor)
      .subscribe(data => console.log(data), error => console.log(error));
    this.caregiver = new Caregiver();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/doctor/caregivers']);
  }
}
