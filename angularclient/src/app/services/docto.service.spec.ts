import { TestBed } from '@angular/core/testing';

import { DoctoService } from './docto.service';

describe('DoctoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DoctoService = TestBed.get(DoctoService);
    expect(service).toBeTruthy();
  });
});
