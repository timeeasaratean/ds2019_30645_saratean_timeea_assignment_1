import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  private baseUrl = 'http://localhost:8080/patient';

  constructor(private http: HttpClient) { }

  getPatient(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
  getMyCaregiver(id: number): Observable<any>{
    return this.http.get(`${this.baseUrl}/mycaregiver/${id}`);
  }
  getMyIntake(id: number):Observable<any>{
    return this.http.get(`${this.baseUrl}/myintake/${id}`);
  }
  getMyMedication(id: number):Observable<any>{
    return this.http.get(`${this.baseUrl}/mymedication/${id}`);
  }

}
