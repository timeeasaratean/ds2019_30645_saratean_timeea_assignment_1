import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../models/user";
import {Patient} from "../models/patient";
import {Caregiver} from "../models/caregiver";

@Injectable({
  providedIn: 'root'
})
export class DoctoService {
  private baseUrl = 'http://localhost:8080/doctor';

  constructor(private http: HttpClient) {
  }

  getMedicamentationList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/medicamentationList`);
  }

  createMedication(medication: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}/medicamentationList`, medication);
  }

  updateMedication(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/medicamentationList/${id}`, value);
  }

  getMedication(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/medicamentationList/${id}`);
  }

  deleteMedication(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/medicamentationList/${id}`, {responseType: 'text'});
  }

  getCaregivers(): Observable<any> {
    return this.http.get(`${this.baseUrl}/caregivers`);
  }

  createCaregiver(caregiver: Caregiver,user: User, doctorId:number){
    let newDTO: {caregiverDTO: Caregiver, userDTO: User} = {caregiverDTO: caregiver, userDTO: user};
    return this.http.post<any>(`${this.baseUrl}/caregivers/add/${doctorId}`, newDTO, {params: {doctorId: doctorId + ''}});
  }

  deleteCaregiver(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/caregivers/${id}`, {responseType: 'text'});
  }

  updateCaregiver(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/caregivers/${id}`, value);
  }
  getCaregiver(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/caregivers/${id}`);
  }

  getPatients(): Observable<any> {
    return this.http.get(`${this.baseUrl}/patients`);
  }

  addPatient(patient: Patient,user: User, caregiverId:number){
    let newDTO: {patientDTO: Patient, userDTO: User} = {patientDTO: patient, userDTO: user};
    return this.http.post<any>(`${this.baseUrl}/patients/add/${caregiverId}`, newDTO, {params: {caregiverId: caregiverId + ''}});
  }

  deletepatient(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/patients/${id}`, {responseType: 'text'});
  }

  updatePatient(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/patients/${id}`, value);
  }
  getPatient(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/patients/${id}`);
  }

  getDoctors(): Observable<any> {
    return this.http.get(`${this.baseUrl}/doctors`);
  }
}
