import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CaregiverService {
  private baseUrl = 'http://localhost:8080/caregiver';

  constructor(private http: HttpClient) { }

  getCaregiver(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
  getMyDoctor(id: number): Observable<any>{
    return this.http.get(`${this.baseUrl}/mydoctor/${id}`);
  }
  getMyPatients(id: number): Observable<any>{
    return this.http.get(`${this.baseUrl}/mypatients/${id}`);
  }
}
