import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CaregiverService} from "../services/caregiver.service";
import {Caregiver} from "../models/caregiver";

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {

  id: number;
  caregiver: Caregiver;

  constructor(private route: ActivatedRoute, private router: Router,
              private caregiverService: CaregiverService) {
  }

  ngOnInit() {
    this.caregiver = new Caregiver();

    this.id = parseInt(localStorage.getItem("idUser"));
    this.caregiverService.getCaregiver(this.id)
      .subscribe(data => {
        console.log(data)
        this.caregiver = data;
      }, error => console.log(error));
    this.caregiverService.getMyDoctor(this.id)
      .subscribe(data => {
        console.log(data)
        this.caregiver.doctor=data;
      }, error => console.log(error));
    this.caregiverService.getMyPatients(this.id)
      .subscribe(data=>{
        console.log(data)
        this.caregiver.patientList = data;
        }, error => console.log(error)
      );
  }

}
