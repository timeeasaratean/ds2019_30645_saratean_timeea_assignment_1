import {Component} from '@angular/core';
import {User} from "./models/user";
import {Router} from "@angular/router";
import {AuthenticationService} from "./services/authentication.service";
import {Role} from "./models/role";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  currentUser: User;

  constructor(private router: Router, private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  private isDoctor(): boolean {
    return this.currentUser.role == Role.Doctor;
  }

  private isCaregiver(): boolean {
    return this.currentUser.role == Role.Caregiver;
  }

  private isPatient(): boolean {
    return this.currentUser.role == Role.Patient;
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
