import { Component, OnInit } from '@angular/core';
import {Medication} from "../models/medication";
import {ActivatedRoute, Router} from "@angular/router";
import {DoctoService} from "../services/docto.service";
import {Caregiver} from "../models/caregiver";

@Component({
  selector: 'app-update-caregiver',
  templateUrl: './update-caregiver.component.html',
  styleUrls: ['./update-caregiver.component.css']
})
export class UpdateCaregiverComponent implements OnInit {

  id: number;
  caregiver: Caregiver;
  submitted= false;

  constructor(private route: ActivatedRoute,private router: Router,
              private doctoService: DoctoService) { }

  ngOnInit() {
    this.caregiver = new Caregiver();

    this.id = this.route.snapshot.params['id'];

    this.doctoService.getCaregiver(this.id)
      .subscribe(data => {
        console.log(data)
        this.caregiver = data;
      }, error => console.log(error));
  }

  updateMedication() {
    this.doctoService.updateCaregiver(this.id, this.caregiver)
      .subscribe(data => console.log(data), error => console.log(error));
    this.caregiver = new Caregiver();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.updateMedication();
  }

  gotoList() {
    this.router.navigate(['/doctor/caregivers']);
  }

}
