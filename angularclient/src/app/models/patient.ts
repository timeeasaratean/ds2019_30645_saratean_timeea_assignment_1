import {Caregiver} from "./caregiver";
import {Intake} from "./intake";

export class Patient {
  idPatient: number;
  name: string;
  birthDate: string;
  gender: string;
  address: string;
  medicalRecord: string;
  caregiver: Caregiver;
  intake: Intake;
}
