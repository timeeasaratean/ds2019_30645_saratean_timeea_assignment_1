export class Medication {
  idMedication: number;
  name: string;
  sideEffects: string;
  dosage: string;
}
