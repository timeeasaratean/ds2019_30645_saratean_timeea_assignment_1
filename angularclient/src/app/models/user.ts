import {Role} from "./role";

export class User {
  iduser : number;
  username: string;
  password: string;
  role: Role

}
