export enum Role {
  Patient = "patient",
  Caregiver = "caregiver",
  Doctor = "doctor"
}
