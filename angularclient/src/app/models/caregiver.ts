import {Doctor} from "./doctor";
import {Patient} from "./patient";

export class Caregiver {
  idCaregiver: number;
  name: string;
  birthDate: string;
  gender: string;
  address: string;
  doctor: Doctor;
  patientList: Patient[];
}
