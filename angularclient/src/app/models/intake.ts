import {Medication} from "./medication";

export class Intake {
  startDate: string;
  endDate: string;
  medication: Medication;
}
