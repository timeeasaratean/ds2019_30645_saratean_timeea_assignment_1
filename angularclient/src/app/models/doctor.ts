import {Caregiver} from "./caregiver";

export class Doctor {
  idDoctor: number;
  name: String;
  caregivers: Caregiver[];
}
