import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {AuthGuard} from "./login_security/auth.guard";
import {Role} from "./models/role";
import {LoginComponent} from "./login/login.component";
import {DoctorComponent} from "./doctor/doctor.component";
import {CaregiverComponent} from "./caregiver/caregiver.component";
import {PatientComponent} from "./patient/patient.component";
import {AddMedicationComponent} from "./add-medication/add-medication.component";
import {UpdateMedicationComponent} from "./update-medication/update-medication.component";
import {AddCaregiverComponent} from "./add-caregiver/add-caregiver.component";
import {UpdateCaregiverComponent} from "./update-caregiver/update-caregiver.component";
import {AddPatientComponent} from "./add-patient/add-patient.component";
import {UpdatePatientComponent} from "./update-patient/update-patient.component";

const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'doctor', component: DoctorComponent, canActivate: [AuthGuard], data: {roles: Role.Doctor}},
  {path: 'caregiver', component: CaregiverComponent, canActivate: [AuthGuard], data: {roles: Role.Caregiver}},
  {path: 'patient', component: PatientComponent, canActivate: [AuthGuard], data: {roles: Role.Patient}},
  {path: 'addMedication', component: AddMedicationComponent, canActivate: [AuthGuard], data: {roles: Role.Doctor}},
  {path: 'updateMedication/:id', component: UpdateMedicationComponent, canActivate: [AuthGuard], data: {roles: Role.Doctor}},
  {path: 'addCaregiver', component: AddCaregiverComponent, canActivate: [AuthGuard], data: {roles: Role.Doctor}},
  {path: 'updateCaregiver/:id', component: UpdateCaregiverComponent, canActivate: [AuthGuard], data: {roles: Role.Doctor}},
  {path: 'addPatient', component: AddPatientComponent, canActivate: [AuthGuard], data: {roles: Role.Doctor}},
  {path: 'updatePatient/:id', component: UpdatePatientComponent, canActivate: [AuthGuard], data: {roles: Role.Doctor}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
