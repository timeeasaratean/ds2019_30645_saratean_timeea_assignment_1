import {Component, OnInit} from '@angular/core';
import {Medication} from "../models/medication";
import {Observable} from "rxjs";
import {DoctoService} from "../services/docto.service";
import {Router} from "@angular/router";
import {Caregiver} from "../models/caregiver";
import {Patient} from "../models/patient";

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {

  medicamentation: Observable<Medication[]>;
  caregivers: Observable<Caregiver[]>;
  patients: Observable<Patient[]>;

  constructor(private doctoService: DoctoService,
              private router: Router) {
  }

  ngOnInit() {
    this.reloadMedication();
    this.reloadCaregivers();
    this.reloadPatients();
  }

  reloadMedication() {
    this.medicamentation = this.doctoService.getMedicamentationList();
  }
  reloadCaregivers(){
    this.caregivers = this.doctoService.getCaregivers();
  }
  reloadPatients(){
    this.patients = this.doctoService.getPatients();
  }

  deleteMedication(id: number) {
    this.doctoService.deleteMedication(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadMedication();
        },
        error => console.log(error));
  }

  updateMedication(id: number) {
    console.log(id);
    this.router.navigate(['/updateMedication', id]);
  }

  deleteCaregiver(id: number) {
    this.doctoService.deleteCaregiver(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadPatients();
        },
        error => console.log(error));
  }

  updateCaregiver(id: number) {
    console.log(id);
    this.router.navigate(['/updateCaregiver', id]);
  }
  deletePatient(id: number) {
    this.doctoService.deletepatient(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadCaregivers();
        },
        error => console.log(error));
  }

  updatePatient(id: number) {
    console.log(id);
    this.router.navigate(['/updatePatient', id]);
  }
}
