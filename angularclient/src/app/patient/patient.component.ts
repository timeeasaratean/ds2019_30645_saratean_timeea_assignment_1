import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Patient} from "../models/patient";
import {PatientService} from "../services/patient.service";

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  id: number;
  patient: Patient;

  constructor(private route: ActivatedRoute, private router: Router,
              private patientService: PatientService) {
  }

  ngOnInit() {
    this.patient = new Patient();

    this.id = parseInt(localStorage.getItem("idUser"));
    this.patientService.getPatient(this.id)
      .subscribe(data => {
        console.log(data)
        this.patient = data;
      }, error => console.log(error));
    this.patientService.getMyCaregiver(this.id)
      .subscribe(data => {
        console.log(data)
        this.patient.caregiver = data;
      }, error => console.log(error));
    this.patientService.getMyIntake(this.id)
      .subscribe(data => {
        console.log(data)
        this.patient.intake = data;
      }, error => console.log(error));
    this.patientService.getMyMedication(this.id)
      .subscribe(data => {
        console.log(data)
        this.patient.intake.medication= data;
      }, error => console.log(error));
  }

}
