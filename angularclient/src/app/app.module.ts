import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {AppRoutingModule} from './/app-routing.module';
import {HomeComponent} from './home/home.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './login/login.component';
import {DoctorComponent} from './doctor/doctor.component';
import {CaregiverComponent} from './caregiver/caregiver.component';
import {PatientComponent} from './patient/patient.component';
import {AuthenticationService} from "./services/authentication.service";
import {AuthGuard} from "./login_security/auth.guard";
import {CaregiverService} from "./services/caregiver.service";
import {PatientService} from "./services/patient.service";
import {DoctoService} from "./services/docto.service";
import {AddMedicationComponent} from './add-medication/add-medication.component';
import {UpdateMedicationComponent} from './update-medication/update-medication.component';
import {AddCaregiverComponent} from './add-caregiver/add-caregiver.component';
import { UpdateCaregiverComponent } from './update-caregiver/update-caregiver.component';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { UpdatePatientComponent } from './update-patient/update-patient.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    DoctorComponent,
    CaregiverComponent,
    PatientComponent,
    AddMedicationComponent,
    UpdateMedicationComponent,
    AddCaregiverComponent,
    UpdateCaregiverComponent,
    AddPatientComponent,
    UpdatePatientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [AuthenticationService, AuthGuard, CaregiverService, PatientService, DoctoService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
